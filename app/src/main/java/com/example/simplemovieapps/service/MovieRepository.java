package com.example.simplemovieapps.service;

import com.example.simplemovieapps.model.movie.MovieDiscoverResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieRepository {
    @GET("3/discover/movie?api_key=05faacecb1bb8a123ad56542b1708bad")
    Call<MovieDiscoverResponse> getMovieDiscover();
}
